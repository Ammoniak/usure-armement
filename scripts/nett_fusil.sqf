﻿//par Eldarius & Ammoniak
//===============================Nettoyage de son fusil============================
		_armepri = primaryWeapon player;
		var_net_arme = 0;
		if (player hasWeapon _armepri) then {

	//Récup variable taux usure
		_tir_fusil = profileNamespace getVariable ["UC_"+serverName+"_"+missionName+"_usure_fusil",0];
	
	//arme principal en main
		player selectWeapon _armepri;
	
	//affichage de la barre de progression
		[(UC_usure_temp_nettayage_fusil*_tir_fusil), var_net_arme == 1,"","", format ["Arme en cour de nettoyage"]] call ace_common_fnc_progressBar;

		hint "Nettoyage de l'arme en cours";
		execVM "scripts\annim_fusil.sqf";
	//creation de l'action ace "annuler Nettoyer son fusil" dans le menu "Nettoyer ses armes"
		
		(findDisplay 46) displayAddEventHandler ["KeyDown", "if ((_this select 1) == 219) then {var_net_arme = 1};"]; //annule l'action si presse touche windows
		(findDisplay 46) displayAddEventHandler ["KeyDown", "if ((_this select 1) == 1) then {var_net_arme = 1};"]; //annule l'action si presse touche echap

		_counter = _tir_fusil; 
		
		for "_i" from 1 to _counter do {
			if (var_net_arme == 0) then {
		
				_tir_fusil = profileNamespace getVariable ["UC_"+serverName+"_"+missionName+"_usure_fusil",0];
				profileNamespace setVariable ["UC_"+serverName+"_"+missionName+"_usure_fusil",_tir_fusil - 1];
				_array1 = "%";
         		hint format["%1 %2",[(_tir_fusil*UC_usure_pourcent_texte_fusil), 1] call BIS_fnc_cutDecimals, _array1];
				sleep UC_usure_temp_nettayage_fusil;
			}
		};
		var_net_arme = 1;
		hint "Nettoyage de l'arme terminer";
		sleep 2;
		hint "";
	} 
	else {
		hint "Vous n'avez pas d'arme pricipal sur vous!";
		sleep 2;
		hint "";
	};
//========================================END======================================
