//par Eldarius & Ammoniak
//=======================================reglages==============================================

	UC_usure_nb_tir_fusil = [1800, 2050, 2400]; //l'enrayage se produirat si la valeur (nombre de coup tiré par l'arme) est entre 700 & 1200
	UC_usure_nb_tir_pistolet = [80, 110, 140]; //l'enrayage se produirat si la valeur (nombre de coup tiré par l'arme) est entre 50 & 70
	UC_usure_nb_tir_lanceur = [20, 25, 30]; //l'enrayage se produirat si la valeur (nombre de coup tiré par l'arme) est entre 10 & 16
	UC_temp_nettoyage = 600; //Temp que dure le nettayage quand 100% sale

//==========================================END================================================

//============================variable syncro (ne pas toucher)=================================

   UC_usure_pourcent_texte_fusil = 100 / (UC_usure_nb_tir_fusil select 2); 
   UC_usure_pourcent_texte_pistolet = 100 / (UC_usure_nb_tir_pistolet select 2); 
	UC_usure_pourcent_texte_lanceur = 100 / (UC_usure_nb_tir_lanceur select 2);
   UC_usure_temp_nettayage_fusil = UC_temp_nettoyage / (UC_usure_nb_tir_fusil select 2) ;// 600(10min)/1200(coup tiré max pour 100% de salissure) = 0.5
	UC_usure_temp_nettayage_pistolet = UC_temp_nettoyage / (UC_usure_nb_tir_pistolet select 2);   // 600(10min)/70(coup tiré max pour 100% de salissure) = 8.57
	UC_usure_temp_nettayage_lanceur = UC_temp_nettoyage / (UC_usure_nb_tir_lanceur select 2); // 600(10min)/16(coup tiré max pour 100% de salissure) = 37.5

//==========================================END================================================

//====================================FONCTION START===========================================

	//Lecture des fonction d'enrayage
	call compile preprocessFileLineNumbers "fonctions\fnc_entretien.sqf";
	//exécution des fonction d'enrayage
	[] spawn fnc_enray_fusil;
	[] spawn fnc_enray_pistolet;
	[] spawn fnc_enray_lanceur;

//==========================================END================================================

//==================================MENU ACE SUR JOUEUR arme===================================

   //creation du sous menu ace "Nettoyer ses armes" 
      _action1 = ["Nettoyersesarmes","Nettoyer ses armes","",{
      },
      {true}] call ace_interact_menu_fnc_createAction;

      [player, 1, ["ACE_SelfActions"], _action1] call ace_interact_menu_fnc_addActionToObject;


   //creation de l'action ace "Nettoyer son fusil" dans le menu "Nettoyer ses armes"
      _action2 = ["Nettoyersonfusil ","Nettoyer son fusil","",{
         execVM "scripts\nett_fusil.sqf";
      },
      {true}] call ace_interact_menu_fnc_createAction;

      [player, 1, ["ACE_SelfActions","Nettoyersesarmes"], _action2] call ace_interact_menu_fnc_addActionToObject;


   //creation de l'action ace "Controler l'état du fusil" dans le menu "Nettoyer ses armes"
      _action2_2 = ["Controlerlétatdufusil ","Controler l'état du fusil","",{
         ["UC_"+serverName+"_"+missionName+"_usure_fusil", primaryWeapon player, UC_usure_pourcent_texte_fusil]  call fnc_controle_salete;
      },
      {true}] call ace_interact_menu_fnc_createAction;

      [player, 1, ["ACE_SelfActions","Nettoyersesarmes"], _action2_2] call ace_interact_menu_fnc_addActionToObject;


   //creation de l'action ace "Nettoyer son pistolet" dans le menu "Nettoyer ses armes"
      _action3 = ["Nettoyersonpistolet","Nettoyer son pistolet","",{
         execVM "scripts\nett_pistol.sqf";
      },
      {true}] call ace_interact_menu_fnc_createAction;

      [player, 1, ["ACE_SelfActions","Nettoyersesarmes"], _action3] call ace_interact_menu_fnc_addActionToObject;


   //creation de l'action ace "Controler l'état du pistolet" dans le menu "Nettoyer ses armes"
      _action3_2 = ["Controlerlétatdupistolet ","Controler l'état du pistolet","",{
         ["UC_"+serverName+"_"+missionName+"_usure_pistolet", handgunWeapon player , UC_usure_pourcent_texte_pistolet]  call fnc_controle_salete;
      },
      {true}] call ace_interact_menu_fnc_createAction;

      [player, 1, ["ACE_SelfActions","Nettoyersesarmes"], _action3_2] call ace_interact_menu_fnc_addActionToObject;


   //creation de l'action ace "Nettoyer son lanceur" dans le menu "Nettoyer ses armes"
      _action4 = ["Nettoyersonlanceur","Nettoyer son lanceur","",{
         execVM "scripts\nett_lanceur.sqf";
      },
      {true}] call ace_interact_menu_fnc_createAction;

      [player, 1, ["ACE_SelfActions","Nettoyersesarmes"], _action4] call ace_interact_menu_fnc_addActionToObject;


   //creation de l'action ace "Controler l'état du lanceur" dans le menu "Nettoyer ses armes"
      _action4_2 = ["Controlerlétatdulanceur ","Controler l'état du lanceur","",{
         ["UC_"+serverName+"_"+missionName+"_usure_lanceur", secondaryWeapon player, UC_usure_pourcent_texte_lanceur]  call fnc_controle_salete;
      },
      {true}] call ace_interact_menu_fnc_createAction;

      [player, 1, ["ACE_SelfActions","Nettoyersesarmes"], _action4_2] call ace_interact_menu_fnc_addActionToObject;

//==========================================END================================================

//=========================action vérifier l'état de l'arme par qqun (a tester)================

   //creation du sous menu ace "controler les armes" 
      _action5 = ["Présentezcesarmes","Présentez ces armes","",{
      },
      {true}] call ace_interact_menu_fnc_createAction;

      [player, 1, ["ACE_SelfActions"], _action5] call ace_interact_menu_fnc_addActionToObject;

   //creation de l'action ace "Controler l'état du fusil" dans le menu "Nettoyer ses armes"
      _action5_1 = ["Présentezsonfusil ","Présentez son fusil","",{
         ["UC_"+serverName+"_"+missionName+"_usure_fusil", primaryWeapon player, UC_usure_pourcent_texte_fusil]  call fnc_presentation_arme;
      },
      {true}] call ace_interact_menu_fnc_createAction;

      [player, 1, ["ACE_SelfActions","Présentezcesarmes"], _action5_1] call ace_interact_menu_fnc_addActionToObject;

   //creation de l'action ace "Controler l'état du pistolet" dans le menu "Nettoyer ses armes"
      _action5_2 = ["Présentezsonpistolet ","Présentez son pistolet","",{
         ["UC_"+serverName+"_"+missionName+"_usure_pistolet", handgunWeapon player , UC_usure_pourcent_texte_pistolet]  call fnc_presentation_arme;
      },
      {true}] call ace_interact_menu_fnc_createAction;

      [player, 1, ["ACE_SelfActions","Présentezcesarmes"], _action5_2] call ace_interact_menu_fnc_addActionToObject;

   //creation de l'action ace "Controler l'état du lanceur" dans le menu "Nettoyer ses armes"
      _action5_3 = ["Présentezsonlanceur ","Présentez son lanceur","",{
         ["UC_"+serverName+"_"+missionName+"_usure_lanceur", secondaryWeapon player, UC_usure_pourcent_texte_lanceur]  call fnc_presentation_arme;
      },
      {true}] call ace_interact_menu_fnc_createAction;

      [player, 1, ["ACE_SelfActions","Présentezcesarmes"], _action5_3] call ace_interact_menu_fnc_addActionToObject;

//==========================================END================================================
