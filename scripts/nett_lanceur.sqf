﻿//par Eldarius & Ammoniak
//===================Nettoyage de son lanceur par Eldarius & Ammoniak===============
		_armelan = secondaryWeapon player;
		var_net_arme = 0;
		if (player hasWeapon _armelan) then {

	//Récup variable taux usure
		_tir_lanceur = profileNamespace getVariable ["UC_"+serverName+"_"+missionName+"_usure_lanceur",0];

	//arme lanceur en main
		player selectWeapon _armelan;

	//affichage de la barre de progression
		[(UC_usure_temp_nettayage_lanceur*_tir_lanceur), var_net_arme == 1,"","", format ["Arme en cour de nettoyage"]] call ace_common_fnc_progressBar;

		hint "Nettoyage du lanceur en cour";
		execVM "scripts\annim_lanceur.sqf";
	//creation de l'action ace "annuler Nettoyer son fusil" dans le menu "Nettoyer ses armes"

		(findDisplay 46) displayAddEventHandler ["KeyDown", "if ((_this select 1) == 219) then {var_net_arme = 1};"]; //annule l'action si presse touche windows
		(findDisplay 46) displayAddEventHandler ["KeyDown", "if ((_this select 1) == 1) then {var_net_arme = 1};"]; //annule l'action si presse touche echap

		_counter = _tir_lanceur;

		for "_i" from 1 to _counter do {
			if (var_net_arme == 0) then {

				_tir_lanceur = profileNamespace getVariable ["UC_"+serverName+"_"+missionName+"_usure_lanceur",0];
				profileNamespace setVariable ["UC_"+serverName+"_"+missionName+"_usure_lanceur",_tir_lanceur - 1];
				_array1 = "%";
				hint format["%1 %2",[(_tir_lanceur*UC_usure_pourcent_texte_lanceur), 1] call BIS_fnc_cutDecimals, _array1];
				sleep UC_usure_temp_nettayage_lanceur;
			};
		};
		var_net_arme = 1;
		hint "Nettoyage du lanceur terminer";
		sleep 2;
		hint "";
	} 
	else {
		hint "Vous n'avez pas de lanceur sur vous!";
		sleep 2;
		hint "";
	};
//========================================END======================================