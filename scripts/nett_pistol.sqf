﻿//par Eldarius & Ammoniak
//===========================Nettoyage de son pistolet=============================
		_armeseg = handgunWeapon player;
		var_net_arme = 0;
		if (player hasWeapon _armeseg) then {

	//Récup variable taux usure
		_tir_pistolet = profileNamespace getVariable ["UC_"+serverName+"_"+missionName+"_usure_pistolet",0];
		
	//arme pistolet en main
		player selectWeapon _armeseg;

	//affichage de la barre de progression
		[(UC_usure_temp_nettayage_pistolet*_tir_pistolet), var_net_arme == 1,"","", format ["Arme en cour de nettoyage"]] call ace_common_fnc_progressBar;

		hint "Nettoyage de l'arme en cour";
		execVM "scripts\annim_pistol.sqf";
	//creation de l'action ace "annuler Nettoyer son fusil" dans le menu "Nettoyer ses armes"

		(findDisplay 46) displayAddEventHandler ["KeyDown", "if ((_this select 1) == 219) then {var_net_arme = 1};"]; //annule l'action si presse touche windows
		(findDisplay 46) displayAddEventHandler ["KeyDown", "if ((_this select 1) == 1) then {var_net_arme = 1};"]; //annule l'action si presse touche echap

		_counter = _tir_pistolet;

		for "_i" from 1 to _counter do {
			if (var_net_arme == 0) then {

				_tir_pistolet = profileNamespace getVariable ["UC_"+serverName+"_"+missionName+"_usure_pistolet",0];
				profileNamespace setVariable ["UC_"+serverName+"_"+missionName+"_usure_pistolet",_tir_pistolet -1];
				_array1 = "%";
				hint format["%1 %2",[(_tir_pistolet*UC_usure_pourcent_texte_pistolet), 1] call BIS_fnc_cutDecimals, _array1];
				sleep UC_usure_temp_nettayage_pistolet;  
			};
		};
		var_net_arme = 1;
		hint "Nettoyage de l'arme terminer";
		sleep 2;
		hint "";
	} 
	else {
		hint "Vous n'avez pas d'armes segondaire sur vous!";
		sleep 2;
		hint "";
	};
//========================================END======================================