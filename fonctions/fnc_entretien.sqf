//par Eldarius & Ammoniak
//=======================================ENRAYAGE==============================
   //Fonction du nettoyage de ses armes
   fnc_enray_fusil = {
      _var_enrayf = player addEventHandler["FiredMan",{  

         _tir_fusil = profileNamespace getVariable ["UC_"+serverName+"_"+missionName+"_usure_fusil",0];

         _unit = (_this select 0);  //object  
         _weapon  = (_this select 1); //string  
         _muzzle = (_this select 2); //string  
         _mode = (_this select 3); //string  
         _ammo =  (_this select 4); //string  
         _magazine =  (_this select 5); //string  
         _projectile = (_this select 6); //object  
         _gunner = (_this select 7); //object  

         if (_weapon isEqualTo (primaryWeapon player)) then {
            //0.1 on augment de 0.2% à chaque tir (il faut 350 tirs pour commencer à enrayer l'arme et 500 pour bloquer l'arme) 
            profileNamespace setVariable ["UC_"+serverName+"_"+missionName+"_usure_fusil",_tir_fusil + 1];
            _tir_fusil = profileNamespace getVariable ["UC_"+serverName+"_"+missionName+"_usure_fusil",0];
         };
         //A partir de 70% de saleté, l'arme à une chance sur 30 de s'enrayer. 
         //Plus le taux de saleté augmente plus les risques de s'enrayer augmente 
         if (random UC_usure_nb_tir_fusil < _tir_fusil) then { 
            [player, primaryWeapon player] call ace_overheating_fnc_jamWeapon; 
         }; 
      }]; 
   };


   fnc_enray_pistolet = {  

      _var_enrayp = player addEventHandler["FiredMan",{  

          _tir_pistolet = profileNamespace getVariable ["UC_"+serverName+"_"+missionName+"_usure_pistolet",0];

         _unit = (_this select 0);  //object  
         _weapon  = (_this select 1); //string  
         _muzzle = (_this select 2); //string  
         _mode = (_this select 3); //string  
         _ammo =  (_this select 4); //string  
         _magazine =  (_this select 5); //string  
         _projectile = (_this select 6); //object  
         _gunner = (_this select 7); //object  

         if (_weapon isEqualTo (handgunWeapon player)) then {
            //0.1 on augment de 0.2% à chaque tir (il faut 350 tirs pour commencer à enrayer l'arme et 500 pour bloquer l'arme) 
            profileNamespace setVariable ["UC_"+serverName+"_"+missionName+"_usure_pistolet",_tir_pistolet + 1];
            _tir_pistolet = profileNamespace getVariable ["UC_"+serverName+"_"+missionName+"_usure_pistolet",0];
         };
         
         //A partir de 70% de saleté, l'arme à une chance sur 30 de s'enrayer. 
         //Plus le taux de saleté augmente plus les risques de s'enrayer augmente 
         if (random UC_usure_nb_tir_pistolet < _tir_pistolet) then { 
            [player, handgunWeapon player] call ace_overheating_fnc_jamWeapon; 
         }; 
      }]; 
   };


   fnc_enray_lanceur = {


      _var_enrayl = player addEventHandler["FiredMan",{  

         _tir_lanceur = profileNamespace getVariable ["UC_"+serverName+"_"+missionName+"_usure_lanceur",0];

         _unit = (_this select 0);  //object  
         _weapon  = (_this select 1); //string  
         _muzzle = (_this select 2); //string  
         _mode = (_this select 3); //string  
         _ammo =  (_this select 4); //string  
         _magazine =  (_this select 5); //string  
         _projectile = (_this select 6); //object  
         _gunner = (_this select 7); //object  

         if (_weapon isEqualTo (secondaryWeapon player)) then {
            //0.1 on augment de 0.2% à chaque tir (il faut 350 tirs pour commencer à enrayer l'arme et 500 pour bloquer l'arme) 
            profileNamespace setVariable ["UC_"+serverName+"_"+missionName+"_usure_lanceur",_tir_lanceur + 1];
            _tir_lanceur = profileNamespace getVariable ["UC_"+serverName+"_"+missionName+"_usure_lanceur",0];
         };
         
         //A partir de 70% de saleté, l'arme à une chance sur 30 de s'enrayer. 
         //Plus le taux de saleté augmente plus les risques de s'enrayer augmente 
         if (random UC_usure_nb_tir_lanceur < _tir_lanceur) then { 
            [player, secondaryWeapon player] call ace_overheating_fnc_jamWeapon; 
         }; 
      }]; 
   };
//==========================================END================================

//=====================================PROGRES BARRE===========================
   //Barre de progression du nettoyage de son pistolet
   fnc_prog = {
      params["_VarNamespace", "_TimeSleep"];
      with uiNamespace do {
         my_awesome_progressBar = findDisplay 46 ctrlCreate ["RscProgress", -1];
         my_awesome_progressBar ctrlSetPosition [ 0.345, 1 ];
         my_awesome_progressBar progressSetPosition 0;
         my_awesome_progressBar ctrlCommit 0;
         my_awesome_progressBar ctrlSetTextColor [1,1,1,1];
      };

      _counter = profileNamespace getVariable [_VarNamespace,0];
      for "_i" from 1 to (_counter*100) do {
         if (var_net_arme == 0) then {
            (uiNamespace getVariable "my_awesome_progressBar") progressSetPosition (_i/(_counter*100));

            //hintSilent format ["%1%2", _i * 100 /_counter, "%"]; // debugg //shows 0-100% now ;)
            sleep (_TimeSleep/100); // interval
         };
      };
      ctrlDelete (uiNamespace getVariable "my_awesome_progressBar");
   };
//==========================================END================================

//================================CONTROLE de la saleter arme==================
   //Contrôle de l'état de son fusil
   fnc_controle_salete = {
      params ["_usure_arme", "_type_arme", "_var_pourcent_texte_arme"];

      _tir = profileNamespace getVariable [_usure_arme,0];

      if (player hasWeapon _type_arme) then {
         _array1 = "%";
         hint format["%1 %2",[(_tir*_var_pourcent_texte_arme), 1] call BIS_fnc_cutDecimals, _array1];
      } 
      else {
         hint "Vous n'avez pas cette arme sur vous!";
         
      };
   };
//==========================================END================================

//================================Presentation de l'arme==================
   //Présenter le niveau de propreté de son arme à un gradé
   fnc_presentation_arme = {
      params ["_usure_arme", "_type_arme", "_var_pourcent_texte_arme"];

      _tir = profileNamespace getVariable [_usure_arme,0];

      if (player hasWeapon _type_arme) then {
         _array1 = "%";
         _messageroll = format["%1 %2 %3", _type_arme, [(_tir*_var_pourcent_texte_arme), 1] call BIS_fnc_cutDecimals, _array1];
	      [player, _messageroll] remoteExec ["globalChat", 0];
      } 
      else {
         hint "Vous n'avez pas cette arme sur vous!";
         
      };
   };
//==========================================END================================